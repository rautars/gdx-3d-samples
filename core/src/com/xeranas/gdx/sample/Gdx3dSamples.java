package com.xeranas.gdx.sample;

import com.badlogic.gdx.Game;
import com.xeranas.gdx.sample.level.EntryScreen;

public class Gdx3dSamples extends Game {

	public Gdx3dSamples() {
		super();
	}
	
	@Override
	public void create() {
		setScreen(new EntryScreen(this));
	}

}
