package com.xeranas.gdx.sample.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.xeranas.gdx.sample.utils.LevelController;

public class ActionPanel implements Disposable {

	public static final float WIDTH_SCALE = 800f;
	public static final float HEIGHT_SCALE = 600f;
	
	private OrthographicCamera camera;
	private Stage stage;
	private Label fpsLabel;
	private Skin skin;
	private LevelController levelController;
	
	public ActionPanel(LevelController levelController) {
		this.levelController = levelController;
		
		stage = new Stage(new FitViewport(WIDTH_SCALE, HEIGHT_SCALE));
		camera = ((OrthographicCamera) stage.getCamera());
		Gdx.input.setInputProcessor(stage);
		
		skin = new Skin(Gdx.files.internal("assets/gdx_test_data/uiskin.json"));
		fpsLabel = new Label("fps:", skin);
		
		Window window = new Window("Menu", skin);
		window.setWidth(WIDTH_SCALE);
		window.row();
		window.add(createSelectBox()).maxWidth(200).align(Align.left);
		window.add(fpsLabel).colspan(4);
		stage.addActor(window);
	}
	
	private SelectBox<String> createSelectBox() {
		final SelectBox<String> selectBox = new SelectBox<String>(skin);
		selectBox.getStyle().listStyle.selection.setRightWidth(10);
		selectBox.getStyle().listStyle.selection.setLeftWidth(20);
		selectBox.setSelected(LevelController.levels()[0]);
		selectBox.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				levelController.switchScreen(selectBox.getSelected());
			}
		});
		selectBox.setItems(LevelController.levels());
		
		return selectBox;
	}
	
	static public class SelectBoxStyle {
		public NinePatch background;
		public NinePatch listBackground;
		public NinePatch listSelection;
		public BitmapFont font;
		public Color fontColor = new Color(1, 1, 1, 1);
		public float itemSpacing = 10;
	}
	
	public void draw() {
		fpsLabel.setText("fps: " + Gdx.graphics.getFramesPerSecond());
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}
}
