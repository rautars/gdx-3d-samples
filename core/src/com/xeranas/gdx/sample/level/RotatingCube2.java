package com.xeranas.gdx.sample.level;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.xeranas.gdx.sample.model.BoxModel;
import com.xeranas.gdx.sample.scene2d.ActionPanel;
import com.xeranas.gdx.sample.scene2d.ActionPanelActor;

public class RotatingCube2 extends ApplicationAdapter {

	private PerspectiveCamera camera;
	private ModelBatch modelBatch;
	private ModelInstance boxInstance;
	private Environment environment;
	private BoxModel boxModel;

	private ActionPanel actionPanel;
	
	@Override
	public void create() {

		camera = new PerspectiveCamera(75, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		camera.position.set(0f, 5f, 3f);
		camera.lookAt(0f, 0f, 0f);
		camera.near = 0.1f;
		camera.far = 300.0f;

		modelBatch = new ModelBatch();
		boxModel = new BoxModel(Color.PINK);
		boxInstance = boxModel.getBoxInstance();

		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 1f, 1f, 1f, 1.0f));
		
	}

	@Override
	public void dispose() {
		modelBatch.dispose();
		boxModel.dispose();
		actionPanel.dispose();
	}

	@Override
	public void render() {
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		camera.rotateAround(Vector3.Zero, new Vector3(0, 1, 0), 1f);
		camera.update();
		modelBatch.begin(camera);
		modelBatch.render(boxInstance, environment);
		modelBatch.end();
		
		actionPanel.draw();
	}

}
