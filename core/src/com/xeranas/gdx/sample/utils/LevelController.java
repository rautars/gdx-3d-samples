package com.xeranas.gdx.sample.utils;

import com.badlogic.gdx.Game;
import com.xeranas.gdx.sample.level.EntryScreen;
import com.xeranas.gdx.sample.level.RotatingCube;

public class LevelController {
	// level names
	private static final String ENTRY = "Entry";
	private static final String ROTATING_CUBE = "Rotating Cube";
	
	private Game game;
	
	private static final String[] LEVELS = new String[]{
			ENTRY,
			ROTATING_CUBE};
	
	public LevelController(Game game) {
		this.game = game;
	}
	
	public static String[] levels() {
		return LEVELS;
	}
	
	public void switchScreen(String levelName) {
		if (ENTRY.equals(levelName)) {
			game.setScreen(new EntryScreen(game));
		}

		if (ROTATING_CUBE.equals(levelName)) {
			game.setScreen(new RotatingCube(game));
		}
	}
}
