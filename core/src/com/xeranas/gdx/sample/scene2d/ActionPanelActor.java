package com.xeranas.gdx.sample.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ActionPanelActor extends Actor {
	private Sprite sprite;
	
	public ActionPanelActor(Camera camera) {
		Texture texture = new Texture(Gdx.files.internal("assets/nav_panel.png"));
		sprite = new Sprite(texture);
		sprite.setPosition(camera.position.x - ActionPanel.WIDTH_SCALE / 2f,
				camera.position.y - ActionPanel.HEIGHT_SCALE / 2f);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		sprite.draw(batch);
	}
}
