package com.xeranas.gdx.sample.level;

import com.badlogic.gdx.Game;
import com.xeranas.gdx.sample.utils.SampleUtil;

public class EntryScreen extends BaseScreen {
	
	public EntryScreen(Game game) {
		super(game);
	}
	
	@Override
	public void render (float delta) {
		SampleUtil.clearBG();
		actionPanel.draw();
	}
	
	
}
